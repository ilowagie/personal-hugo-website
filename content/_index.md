---
linktitle: "Home/About"
date: 2020-02-06
---

Hello, my name is Inigo Lowagie. I was born 1996 in Ghent.
I studied Ancient Greek and Latin in secondary school and science and electrical engineering at Ghent University.
As you might expect, I have a wide range of interests, though at the time I'm most interested in tech.
On this site you might find some random content either as blog posts or links to git-projects.
Stay a while, read a bit, and if you're interested in hiring me, you can [contact me](mailto:inigo@lowagie.com) for my resume.
