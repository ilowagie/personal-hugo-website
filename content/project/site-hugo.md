---
linktitle: personal-hugo-website
title: personal-hugo-website
gitlink: "https://gitlab.com/ilowagie/personal-hugo-website"
date: 2020-02-15T21:25:15+01:00
logo: gitlab-32px.png
draft: false
technologies:
- hugo
- html
- css
---

{{% mdinclude file="README.md" %}}
