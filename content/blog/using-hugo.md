---
linktitle: "Using Hugo"
title: "/home/ilowagie/blog/using-hugo"
date: 2020-02-28T21:56:07+01:00
tags:
- web
- hugo
categories:
- software
draft: false
---

Before working on this site, I had experience working with [Ruby on Rails](https://rubyonrails.org/) from a university course project and [Symfony 4](https://symfony.com/4) from developing the site of [CenEka](https://ceneka.be), a student organization I was part of.
Both are frameworks that allow you to quickly cobble together a good web application.
So the easiest way to develop my own website would've been to use the Symfony framework since that would be the framework that I have the most experience with.
But I chose not to.
There would be nothing new to learn by again using Symfony.
I like to make things difficult for myself.

# Static versus Dynamic

There were a few things that this site needed to be.
Most importantly it had to be snappy to hopefully show that I am somewhat competent as an engineer.
It also should be as minimal as possible without sacrificing looks or functionality.
But it needs to be easy to maintain too.

To get the snappiness and minimalism, a static site seemed to be the way to go.
For the purpose that the site would be used, any server-side processing would be unnecessary bloat.
It is a necessary evil for the [CenEka website](https://ceneka.be), as the ability for students to register and subscribe to events seems fundamental for a site of a student organization.
The trade-off is that it might take a while for the site to load, especially if you try to reach it from outside Europe.

The only thing needed to load a static site are a couple of files that might as well be cached close-by.
There is no need to wait for a specific server or servers to finish doing its thing.
An added benefit is that hosting for static sites is mostly free.
So all I needed to do was write some HTML, write some CSS, upload it to something like [Gitlab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) and I'm done.
But who wants to write HTML from scratch without any preprocessing?
I don't.
Work smarter, not harder.

# Hating Hugo

I first heard about Hugo randomly online from someone named [Chris Titus](https://www.christitus.com/) through a [Youtube video](https://www.youtube.com/watch?v=6JaBian3vgI) I happened to stumble upon.
Other than pointing out the advantages of making your site static he argues that Hugo is easy to use.
For the record, it is, for normal people.

His accompanying guide (and many other similar quick-start guides) seemed to be more targeted to people who use Wordpress and the like.
They all start with some sort of template or pre-written website and modify that.
I didn't really feel comfortable doing that.
I wanted to write my site from scratch to make it (hopefully) stand out more and I wanted to force myself to become better at writing CSS.
So I skipped all quick-setup guides and went straight to the documentation.

For some reason I initially had some difficulty parsing the documentation.
Again, this is mostly my fault.
I forced myself to write the site as quick as possible but also from scratch.
The documentation and guides aren't really made for that (and they don't have to).

Then, when I started to slowly get the hang of it I got annoyed by a lot of the choices made by the developers.
A lot of the syntax and naming of variables or even the whole variable system felt pretty counter-intuitive.
I don't remember having the same difficulties learning Ruby on Rails or Symfony!
There were multiple times that I thought: "I got this now!" to then try something new out without reading the documentation only to spectacularly fail and having to read the documentation anyway.
Also, I really hate the date formatting strings, but that is more a complaint for the Go developers I guess.

Anyway, after an embarrassingly long struggle with Hugo, I started to understand it better.
Well, it seemed long anyway.
Eventually it only took a week or two combined with school to arrive at where we are now.

# Loving Hugo

So now, I would recommend hugo for building sites that don't necessarily require server-side processing.
It's easy to install: on Arch Linux, as on most distros I think, it is present in the official repos.
If you're not that fixed on not using pre-build sites from github, it is easy to set-up and start working.
And if you have the same problems as I had with the documentation, you'll get the hang of it eventually.
In hindsight, it's not hard to use at all.
