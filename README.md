# ilowagie's personal website

This is the source for my personal website.
It uses hugo as a static site generator.
The site is for hosting my CV, portfolio and a blog that allows me to rant and ramble about things I care about.
Since there's no need for any server-side processing to do this, the site is made static.
